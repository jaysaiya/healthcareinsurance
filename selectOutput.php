<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "doctors";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$sql = "SELECT * FROM doctors where Doctor_ID = '".$_POST['idnumber']."'";
$result = $conn->query($sql);
?>

<html>

        <head>
                <link rel="stylesheet" href="https://bootswatch.com/4/cerulean/bootstrap.min.css">
                <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
          
              </head>
          

<body>

        <div>
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                      <a class="navbar-brand" href="#">We(k)NoSql HealthCare Insurance</a>
                      <button class="navbar-toggler" aria-expanded="false" aria-controls="navbarColor03" aria-label="Toggle navigation" type="button" data-toggle="collapse" data-target="#navbarColor03">
                        <span class="navbar-toggler-icon"></span>
                      </button>
                    
                      <div class="collapse navbar-collapse" id="navbarColor03">
                        <ul class="navbar-nav mr-auto">
                          <li class="nav-item active">
                            <a class="nav-link" href="select.php">Select<span class="sr-only">(current)</span></a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="insert.php">Insert</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="update.php">Update</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="delete.php">Delete</a>
                          </li>
                        </ul>
                         
                      </div>
                    </nav>
          </div>
          
<div>
     
<?php          
if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        
    ?>
        <table class="table table-hover">
          <thead>
            <tr>
              <th scope="col">Attribute</th>
              <th scope="col">Content</th>
            </tr>
          </thead>
          
          <tbody>
            <tr class="table-Dark">
              <th scope="row">Doctor_Fname</th>
              <td><?php echo $row['Doctor_Fname'];?></td>
            </tr>
            <tr class="table-Dark">
              <th scope="row">Doctor_Lname</th>
              <td><?php echo $row['Doctor_Lname'];?></td>
            </tr>
            <tr class="table-primary">
              <th scope="row">Doctor_Street_No.</th>
              <td><?php echo $row['Doctor_Street_No.'];?></td>
            </tr>
                    <tr class="table-primary">
                    <th scope="row">Doctor_Street_Name</th>
                    <td><?php echo $row['Doctor_Street_Name'];?></td>
            </tr>
            <tr class="table-primary">
                    <th scope="row">Doctor_City</th>
                    <td><?php echo $row['Doctor_City'];?></td>
            </tr>
            <tr class="table-primary">
                    <th scope="row">Doctor_State</th>
                    <td><?php echo $row['Doctor_State'];?></td>
            </tr>
            <tr class="table-primary">
                    <th scope="row">Doctor_Zipcode</th>
                    <td><?php echo $row['Doctor_Zipcode'];?></td>
            </tr>       
            <tr class="table-dark">
              <th scope="row">Doctor_Phone_Personal</th>
              <td><?php echo $row['Doctor_Phone_Personal'];?></td>
            </tr>
            <tr class="table-info">
                    <th scope="row">Doctor_Phone_Office</th>
                    <td><?php echo $row['Doctor_Phone_Office'];?></td>
            </tr>
            <tr class="table-success">
              <th scope="row">Doctor_Email_Address</th>
              <td><?php echo $row['Doctor_Email_Address'];?></td>
            </tr>
            <tr class="table-danger">
              <th scope="row">Doctor_DOB</th>
              <td><?php echo $row['Doctor_DOB'];?></td>
            </tr>
            <tr class="table-warning">
              <th scope="row">Doctor_Education</th>
              <td><?php echo $row['Doctor_Education'];?></td>
            </tr>
            <tr class="table-info">
              <th scope="row">Doctor_Gender</th>
              <td><?php echo $row['Doctor_Gender'];?></td>
            </tr>
            <tr class="table-success">
              <th scope="row">Doctor_Years_of_Exp</th>
              <td><?php echo $row['Doctor_Years_of_Exp'];?></td>
            </tr>
          </tbody>
        </table>
        <div>
<?php    }
} else {
    echo "0 results";
}
$conn->close();
?>
        
              </div>
        
      </div>
      
</body>
    </html>