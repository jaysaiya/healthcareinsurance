<html>

    <?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    
    // Create connection
    $conn = new mysqli($servername, $username, $password);
    
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    } 
    ?>
    
    <head>
      <link rel="stylesheet" href="https://bootswatch.com/4/cerulean/bootstrap.min.css">
      <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>

    </head>

    <body>
<div>
      <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">We(k)NoSql HealthCare Insurance</a>
            <button class="navbar-toggler" aria-expanded="false" aria-controls="navbarColor03" aria-label="Toggle navigation" type="button" data-toggle="collapse" data-target="#navbarColor03">
              <span class="navbar-toggler-icon"></span>
            </button>
          
            <div class="collapse navbar-collapse" id="navbarColor03">
                        <ul class="navbar-nav mr-auto">
                          <li class="nav-item">
                            <a class="nav-link" href="select.php">Select</a>
                          </li>
                          <li class="nav-item active">
                            <a class="nav-link" href="insert.php">Insert <span class="sr-only">(current)</span></a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="update.php">Update</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="delete.php">Delete</a>
                          </li>
                        </ul>
                         
                      </div>
          </nav>
</div>

<div>
    <div class="progress">
      </div>
          <form method = "post" action = "formSubmit.php">
            <fieldset>
              <legend>INSERT</legend>
            </div>
            
              </div>
              <div class="form-group">
                <label for="Doctor_FName">Doctor_Fname</label>
                <input type="varchar" class="form-control" name = "Doctor_FName" id="Doctor_FName" aria-describedby="emailHelp" placeholder="Enter First Name here">
              </div>
              <div class="form-group">
                <label for="Doctor_LName">Doctor_Lname</label>
                <input type="varchar" class="form-control" id="Doctor_LName" name = "Doctor_LName" aria-describedby="emailHelp" placeholder="Enter Last Name here">
              </div>
              <div class="form-group">
                <label for="Dctor_Street_No.">Doctor_Street_No.</label>
                <input type="varchar" class="form-control" id="Dctor_Street_No" name = "Doctor_Street_No" placeholder="Street Number">
              </div>
            </div>
            <div class="form-group">
              <label for="Doctor_Street_Name">Doctor_Street_Name</label>
              <input type="varchar" class="form-control" id="Doctor_Street_Name" name = "Doctor_Street_Name" aria-describedby="emailHelp" placeholder="Enter Street Name">
            </div>
          </div>
          <div class="form-group">
            <label for="Doctor_City">Doctor_City</label>
            <input type="varchar" class="form-control" id="Doctor_City" name = "Doctor_City" aria-describedby="emailHelp" placeholder="Enter City here">
          </div>
        </div>
        <div class="form-group">
          <label for="Doctor_State">Doctor_State</label>
          <input type="varchar" class="form-control" id="Doctor_State" name = "Doctor_State" aria-describedby="emailHelp" placeholder="Enter State here">
        </div>
      </div>
      <div class="form-group">
        <label for="Doctor_Zipcode">Doctor_Zipcode</label>
        <input type="number" class="form-control" id="Doctor_Zipcode" name = "Doctor_Zipcode" aria-describedby="emailHelp" placeholder="Enter Zipcode here">
      </div>
    </div>
    <div class="form-group">
      <label for="Doctor_Phone_Personal">Doctor_Phone_Personal</label>
      <input type="varchar" class="form-control" id="Doctor_Phone_Personal" name = "Doctor_Phone_Personal" aria-describedby="emailHelp" placeholder="Enter Personal Phone here">
    </div>
  </div>
  <div class="form-group">
    <label for="Doctor_Phone_Office">Doctor_Phone_Office</label>
    <input type="varchar" class="form-control" id="Doctor_Phone_Office" name = "Doctor_Phone_Office" aria-describedby="emailHelp" placeholder="Enter Office Phone here">
  </div>
</div>
<div class="form-group">
  <label for="Doctor_Email_Address">Doctor_Email_Address</label>
  <input type="varchar" class="form-control" id="Doctor_Email_Address" name = "Doctor_Email_Address" aria-describedby="emailHelp" placeholder="Enter Email id here">
</div>
</div>
<div class="form-group">
  <label for="Doctor_DOB">Doctor_DOB</label>
  <input type="date" class="form-control" id="Doctor_DOB" name = "Doctor_DOB" aria-describedby="emailHelp" placeholder="Enter Date of Birth here">
</div>
</div>
<div class="form-group">
  <label for="Doctor_Education">Doctor_Education</label>
  <input type="varchar" class="form-control" id="Doctor_Education" name = "Doctor_Education" aria-describedby="emailHelp" placeholder="Enter Education here">
</div>
</div>
<div class="form-group">
  <label for="Doctor_Gender">Doctor_Gender</label>
  <input type="varchar" class="form-control" id="Doctor_Gender" name = "Doctor_Gender" aria-describedby="emailHelp" placeholder="Enter Gender here">
</div>
</div>
<div class="form-group">
  <label for="Doctor_Years_of_Exp">Doctor_Years_of_Exp</label>
  <input type="number" class="form-control" id="Doctor_Years_of_Exp" name = "Doctor_Years_of_Exp" aria-describedby="emailHelp" placeholder="Enter Years of Experience here">
</div>
              
              <div class="form-group">
                <label for="exampleInputFile">File input</label>
                <input type="file" class="form-control-file" id="exampleInputFile" aria-describedby="fileHelp">
                <small id="fileHelp" class="form-text text-muted">This is some placeholder block-level help text for the above input. It's a bit lighter and easily wraps to a new line.</small>
              </div>
              <fieldset class="form-group">
                
                
                <div class="alert alert-dismissible alert-warning">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <h4 class="alert-heading">Warning!</h4>
                    <p class="mb-0">If you click Submit, a new Entry will be inserted in the table. <a href="#" class="alert-link"></a>.</p>
                  </div>
                
              </fieldset>
              <input type="submit" class="btn btn-primary"/>
            </fieldset>
          </form>
          
</div>


    </body>
</htm>