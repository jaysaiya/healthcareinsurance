<html>

        <head>
                <link rel="stylesheet" href="https://bootswatch.com/4/cerulean/bootstrap.min.css">
                <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
          
                <style>
body {background-color: #151B54;  }     

</style>
</head>
          

<body>
      <div>
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                      <a class="navbar-brand" href="#">We(k)NoSql HealthCare Insurance</a>
                      <button class="navbar-toggler" aria-expanded="false" aria-controls="navbarColor03" aria-label="Toggle navigation" type="button" data-toggle="collapse" data-target="#navbarColor03">
                        <span class="navbar-toggler-icon"></span>
                      </button>
                    
                      <div class="collapse navbar-collapse" id="navbarColor03">
                        <ul class="navbar-nav mr-auto">
                          <li class="nav-item active">
                            <a class="nav-link" href="select.php">Select<span class="sr-only">(current)</span></a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="insert.php">Insert</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="update.php">Update</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="delete.php">Delete</a>
                          </li>
                        </ul>
                         
                      </div>
                    </nav>
          </div>


          <br><br>

          <div class="jumbotron">
  <h1 class="display-3">Welcome to our HealthCare Services</h1>
  <p class="lead">Please select the options available in the navbar to test operations</p>
  

</div>

<div class = "container">
<div class= "row">
<div class = "col-4">
<blockquote class="blockquote text-left">
  <p class="mb-0">Access to good health care shouldn't depend on where you live.</p>
  <footer class="blockquote-footer">Victoria Beckham <cite title="Source Title"></cite></footer>
</blockquote>
</div>
<div class = "col-4">
<blockquote class="blockquote text-center">
  <p class="mb-0">Health care must be recognized as a right, not a privilege</p>
  <footer class="blockquote-footer">Bernie Sanders <cite title="Source Title"></cite></footer>
</blockquote>
</div>
<div class = "col-4">
<blockquote class="blockquote text-right">
  <p class="mb-0">Your Health is our Reward</p>
  <footer class="blockquote-footer">Jay, Preetham, Aarsh & Tanvi<cite title="Source Title"></cite></footer>
</blockquote>
</div>

</div>
</div>
    </body>
</html>